import 'package:flutter/material.dart';

final blue = Color(0xff253C95);
final black = Colors.black;
final secondaryColor = Color(0xFF6CA8F1);
final pYellow = Color(0xffF5AF00);
final white = Colors.white;
final dial1 = Colors.blue[400];
final dial2 = Colors.blue[500];
final dial3 = Colors.blue[600];
final dial4 = Colors.blue[700];