import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

final text12n = TextStyle(
  fontSize: 12,
  fontWeight: FontWeight.normal,
);

final text12 = TextStyle(
  fontSize: 12,
  fontWeight: FontWeight.bold,
);

final text15 = TextStyle(
  fontSize: 15,
  fontWeight: FontWeight.bold,
);

final text15n = TextStyle(
  fontSize: 15,
  fontWeight: FontWeight.normal
);

final text20 = TextStyle(
  fontSize: 20,
  fontWeight: FontWeight.bold
);

final text25 = TextStyle(
  fontSize: 22,
  fontWeight: FontWeight.bold
);

final white12 = TextStyle(
  fontSize: 12,
  color: Colors.white,
  fontWeight: FontWeight.bold,
);

final white12n = TextStyle(
  fontSize: 12,
  color: Colors.white,
  fontWeight: FontWeight.normal,
);

final white13 = TextStyle(
  fontSize: 13,
  color: Colors.white,
  fontWeight: FontWeight.bold,
);

final white22 = TextStyle(
  fontSize: 22,
  color: Colors.white,
  fontWeight: FontWeight.bold,
);

final white15 = TextStyle(
  fontSize: 15,
  color: Colors.white,
  fontWeight: FontWeight.bold,
);

final white18 = TextStyle(
  fontSize: 18,
  color: Colors.white,
  fontWeight: FontWeight.bold,
);

final star = TextStyle(
  color: Colors.red
);