import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

final box5 = SizedBox(height: 5);
final box10 = SizedBox(height: 10);
final box15 = SizedBox(height: 15);
final box20 = SizedBox(height: 20);
final box30 = SizedBox(height: 30);
final boxW5 = SizedBox(width: 5);
