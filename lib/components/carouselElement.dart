import 'package:carousel_slider/carousel_slider.dart';
import 'package:derek_masyarakat_v2/components/helper.dart';
import 'package:derek_masyarakat_v2/screens/alur.dart';
import 'package:derek_masyarakat_v2/screens/perda.dart';
import 'package:derek_masyarakat_v2/screens/report.dart';
import 'package:derek_masyarakat_v2/screens/search.dart';
import 'package:derek_masyarakat_v2/styles/style.dart';
import 'package:derek_masyarakat_v2/styles/text.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';

class CarouselElement extends StatefulWidget {
  @override
  _CarouselElementState createState() => _CarouselElementState();
}

class _CarouselElementState extends State<CarouselElement> {
  CarouselSlider carouselSlider;

  int _current = 0;

  String judul1 = "";
  String desc1 = "";
  String judul2 = "";
  String desc2 = "";
  String judul3 = "";
  String desc3 = "";
  String judul4 = "";
  String desc4 = "";
  String judulGambar = "";
  String gambar = "";

  Future getData() async {
    SharedPreferences pref = await SharedPreferences.getInstance();
    if (pref.getString("status") == "ok") {
      setState(() {
        judul1 = pref.getString("judul1");
        desc1 = pref.getString("desc1");
        judul2 = pref.getString("judul2");
        desc2 = pref.getString("desc2");
        judul3 = pref.getString("judul3");
        desc3 = pref.getString("desc3");
        judul4 = pref.getString("judul4");
        desc4 = pref.getString("desc4");
        judulGambar = pref.getString("judulGambar");
        gambar = pref.getString("gambar");
      });
    } else {
      setState(() {
        judul1 = "";
        desc1 = "";
        judul2 = "";
        desc2 = "";
        judul3 = "";
        desc3 = "";
        judul4 = "";
        desc4 = "";
        judulGambar = "";
        gambar = "";
      });
    }
  }

  @override
  void initState() {
    getData();
    super.initState();
  }

  @override
  void dispose() {
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    final boxHome = BoxDecoration(
      borderRadius: BorderRadius.circular(50),
      color: Theme.of(context).accentColor,
    );

    double height = MediaQuery.of(context).size.height;
    double width = MediaQuery.of(context).size.width;

    return Container(
      width: width,
      height: height,
      child: ListView(
        children: [
          Column(
            children: [
              carouselSlider = CarouselSlider(
                height: height / 1.3,
                initialPage: 0,
                enlargeCenterPage: true,
                autoPlay: true,
                reverse: false,
                enableInfiniteScroll: true,
                autoPlayInterval: Duration(seconds: 10), //otomatis geser
                autoPlayAnimationDuration: Duration(milliseconds: 2000),
                pauseAutoPlayOnTouch: Duration(seconds: 10),
                scrollDirection: Axis.horizontal,
                onPageChanged: (index) {
                  setState(() {
                    _current = index;
                  });
                },
                items: [
                  GestureDetector(
                    onTap: () {
                      Navigator.push(
                        context,
                        MaterialPageRoute(
                          builder: (context) => Report(),
                        ),
                      );
                    },
                    child: Container(
                      margin: EdgeInsets.symmetric(horizontal: 5.0),
                      padding: EdgeInsets.all(15),
                      decoration: boxHome,
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.center,
                        crossAxisAlignment: CrossAxisAlignment.center,
                        children: [
                          Image.asset(
                            "images/report.png",
                            width: MediaQuery.of(context).size.width / 8,
                          ),
                          box10,
                          Text(
                            judul3,
                            style: white18,
                            textAlign: TextAlign.center,
                          ),
                          box5,
                          Text(
                            desc3,
                            style: white13,
                            textAlign: TextAlign.center,
                          ),
                        ],
                      ),
                    ),
                  ),
                  GestureDetector(
                    onTap: () {
                      Navigator.push(
                        context,
                        MaterialPageRoute(
                          builder: (context) => Alur(),
                        ),
                      );
                    },
                    child: Container(
                      width: MediaQuery.of(context).size.width,
                      margin: EdgeInsets.symmetric(horizontal: 5.0),
                      padding: EdgeInsets.all(15),
                      decoration: boxHome,
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.center,
                        crossAxisAlignment: CrossAxisAlignment.center,
                        children: [
                          Text(
                            judulGambar,
                            style: white18,
                            textAlign: TextAlign.center,
                          ),
                          box10,
                          Image.network(
                            Helper.infografis() + gambar,
                            width: MediaQuery.of(context).size.height / 3,
                          ),
                        ],
                      ),
                    ),
                  ),
                  GestureDetector(
                    onTap: () {
                      Navigator.push(
                        context,
                        MaterialPageRoute(
                          builder: (context) => Search(),
                        ),
                      );
                    },
                    child: Container(
                      margin: EdgeInsets.symmetric(horizontal: 5.0),
                      padding: EdgeInsets.all(15),
                      decoration: boxHome,
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.center,
                        crossAxisAlignment: CrossAxisAlignment.center,
                        children: [
                          Image.asset(
                            "images/search.png",
                            width: MediaQuery.of(context).size.width / 8,
                          ),
                          box10,
                          Text(
                            judul2,
                            style: white18,
                            textAlign: TextAlign.center,
                          ),
                          box5,
                          Text(
                            desc2,
                            style: white13,
                            textAlign: TextAlign.center,
                          ),
                        ],
                      ),
                    ),
                  ),
                  GestureDetector(
                    onTap: () {
                      Navigator.push(
                        context,
                        MaterialPageRoute(
                          builder: (context) => Perda(),
                        ),
                      );
                    },
                    child: Container(
                      margin: EdgeInsets.symmetric(horizontal: 5.0),
                      padding: EdgeInsets.all(15),
                      decoration: boxHome,
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.center,
                        crossAxisAlignment: CrossAxisAlignment.center,
                        children: [
                          Image.asset(
                            "images/law.png",
                            width: MediaQuery.of(context).size.width / 8,
                          ),
                          box10,
                          Text(
                            judul1,
                            style: white18,
                            textAlign: TextAlign.center,
                          ),
                          box5,
                          Text(
                            desc1,
                            style: white13,
                            textAlign: TextAlign.center,
                          ),
                        ],
                      ),
                    ),
                  ),
                ],
              ),
            ],
          ),
        ],
      ),
    );
  }
}
