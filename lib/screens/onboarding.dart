import 'package:derek_masyarakat_v2/screens/home.dart';
import 'package:derek_masyarakat_v2/styles/color.dart';
import 'package:derek_masyarakat_v2/styles/text.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:shared_preferences/shared_preferences.dart';

class Onboarding extends StatefulWidget {
  @override
  _OnboardingState createState() => _OnboardingState();
}

class _OnboardingState extends State<Onboarding> {
  String judul1 = "";
  String judul2 = ""; //search
  String judul3 = ""; //report
  String desc1 = "";
  String desc2 = "";
  String desc3 = "";

  Future getData() async {
    SharedPreferences pref = await SharedPreferences.getInstance();
    if (pref.getString("status") == "ok") {
      setState(() {
        judul2 = pref.getString("judul2");
        judul3 = pref.getString("judul3");
        desc2 = pref.getString("desc2");
        desc3 = pref.getString("desc3");
      });
    } else {
      setState(() {
        judul2 = "";
        judul3 = "";
        desc2 = "";
        desc3 = "";
      });
    }
  }

  @override
  void initState() {
    getData();
    super.initState();
  }

  @override
  void dispose() {
    super.dispose();
  }

  //indicator active
  final int _numPages = 3;
  final PageController _pageController = PageController(initialPage: 0);
  int _currentPage = 0;

  List<Widget> _buildPageIndicator() {
    List<Widget> list = [];
    for (int i = 0; i < _numPages; i++) {
      list.add(i == _currentPage ? _indicator(true) : _indicator(false));
    }

    return list;
  }

  Widget _indicator(bool isActive) {
    return AnimatedContainer(
      duration: Duration(milliseconds: 150),
      margin: EdgeInsets.symmetric(horizontal: 8.0),
      height: 8.0,
      width: isActive ? 30.0 : 16.0,
      decoration: BoxDecoration(
        color: isActive ? Theme.of(context).accentColor : dial2,
        borderRadius: BorderRadius.all(
          Radius.circular(12),
        ),
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    SystemChrome.setPreferredOrientations(
      [
        DeviceOrientation.portraitUp,
      ],
    );

    return Scaffold(
      body: AnnotatedRegion<SystemUiOverlayStyle>(
        value: SystemUiOverlayStyle.light,
        child: Container(
          child: Padding(
            padding: EdgeInsets.symmetric(vertical: 35.0),
            child: Column(
              children: <Widget>[
                Container(
                  alignment: Alignment.centerRight,
                  child: FlatButton(
                    onPressed: () async {
                      SharedPreferences pref =
                          await SharedPreferences.getInstance();
                      pref.setBool("isSign", true);
                      Navigator.pushAndRemoveUntil(
                        context,
                        MaterialPageRoute(builder: (context) => Home()),
                        (Route<dynamic> route) => false,
                      );
                    },
                    child: Text(
                      'Lewati',
                      style: text20,
                    ),
                  ),
                ),
                Column(
                  children: <Widget>[
                    Container(
                      height: MediaQuery.of(context).size.height / 1.3,
                      child: PageView(
                        physics: ClampingScrollPhysics(),
                        controller: _pageController,
                        onPageChanged: (int page) {
                          setState(
                            () {
                              _currentPage = page;
                            },
                          );
                        },
                        children: <Widget>[
                          //Screen 1
                          Padding(
                            padding: EdgeInsets.only(left: 25, right: 25),
                            child: Column(
                              mainAxisAlignment: MainAxisAlignment.center,
                              crossAxisAlignment: CrossAxisAlignment.center,
                              children: <Widget>[
                                Center(
                                  child: Image(
                                    image: AssetImage('images/smartphone.png'),
                                    height:
                                        MediaQuery.of(context).size.height / 10,
                                  ),
                                ),
                                Center(
                                  child: Padding(
                                    padding: const EdgeInsets.only(top: 50.0),
                                    child: Text(
                                      "SIMDEK",
                                      style: text25,
                                      textAlign: TextAlign.center,
                                    ),
                                  ),
                                ),
                                Center(
                                  child: Padding(
                                    padding: const EdgeInsets.only(top: 15.0),
                                    child: Text(
                                      'Aplikasi SIMDEK (Sistem Informasi Derek) merupakan Aplikasi resmi Dinas Perhubungan Kota Bandung berfungsi untuk memudahkan masyarakat melaporkan pelanggaran parkir liar agar dapat di tindak langsung oleh Petugas di lapangan serta memberi informasi bagi pelanggar parkir yang telah di tindak untuk proses lebih lanjut',
                                      style: text15n,
                                      textAlign: TextAlign.center,
                                    ),
                                  ),
                                ),
                              ],
                            ),
                          ),
                          //End of Screen 1
                          //-------------------------------------------
                          //Screen 2
                          Padding(
                            padding: EdgeInsets.only(left: 25, right: 25),
                            child: Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              mainAxisAlignment: MainAxisAlignment.center,
                              children: <Widget>[
                                Center(
                                  child: Image(
                                    image: AssetImage('images/report.png'),
                                    height: 100,
                                    width: 100,
                                  ),
                                ),
                                Center(
                                  child: Padding(
                                    padding: const EdgeInsets.only(top: 50.0),
                                    child: Text(
                                      judul3,
                                      style: text25,
                                      textAlign: TextAlign.center,
                                    ),
                                  ),
                                ),
                                Center(
                                  child: Padding(
                                    padding: const EdgeInsets.only(top: 15.0),
                                    child: Text(
                                      desc3,
                                      style: text15n,
                                      textAlign: TextAlign.center,
                                    ),
                                  ),
                                ),
                              ],
                            ),
                          ),
                          //End of Screen 2
                          //---------------------------------------------
                          //Screen 3
                          Padding(
                            padding: EdgeInsets.only(left: 25, right: 25),
                            child: Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              mainAxisAlignment: MainAxisAlignment.center,
                              children: <Widget>[
                                Center(
                                  child: Image(
                                    image: AssetImage('images/search.png'),
                                    height: 100,
                                    width: 100,
                                  ),
                                ),
                                Center(
                                  child: Padding(
                                    padding: const EdgeInsets.only(top: 50.0),
                                    child: Text(
                                      judul2,
                                      style: text25,
                                      textAlign: TextAlign.center,
                                    ),
                                  ),
                                ),
                                Center(
                                  child: Padding(
                                    padding: const EdgeInsets.only(top: 15.0),
                                    child: Text(
                                      desc2,
                                      style: text15n,
                                      textAlign: TextAlign.center,
                                    ),
                                  ),
                                ),
                              ],
                            ),
                          ),
                          //End of Screen 3
                        ],
                      ),
                    ),
                  ],
                ),
              ],
            ),
          ),
        ),
      ),
      bottomSheet: _currentPage == _numPages - 1
          ? GestureDetector(
              onTap: () async {
                SharedPreferences pref = await SharedPreferences.getInstance();
                pref.setBool("isSign", true);
                Navigator.pushAndRemoveUntil(
                  context,
                  MaterialPageRoute(builder: (context) => Home()),
                  (Route<dynamic> route) => false,
                );
              },
              child: Container(
                height: 60.0,
                width: double.infinity,
                color: Theme.of(context).accentColor,
                child: Center(
                  child: Text(
                    'Mulai',
                    style: TextStyle(
                      color: white,
                      fontSize: 20.0,
                      fontWeight: FontWeight.bold,
                    ),
                  ),
                ),
              ),
            )
          : Container(
              height: 60.0,
              width: double.infinity,
              // color: Theme.of(context).accentColor,
              child: Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: _buildPageIndicator(),
              ),
            ),
    );
  }
}
