import 'dart:math';

import 'package:derek_masyarakat_v2/components/helper.dart';
import 'package:derek_masyarakat_v2/styles/color.dart';
import 'package:derek_masyarakat_v2/styles/style.dart';
import 'package:derek_masyarakat_v2/styles/text.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_ticket_widget/flutter_ticket_widget.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:http/http.dart' as http;
import 'dart:convert';

import 'package:modal_progress_hud/modal_progress_hud.dart';

class Search extends StatefulWidget {
  @override
  _SearchState createState() => _SearchState();
}

class _SearchState extends State<Search> with SingleTickerProviderStateMixin {
  final _formKey = GlobalKey<FormState>();

  TextEditingController _nopolController = TextEditingController();

  int x;

  String showText =
      "Tekan tombol disebelah kiri dan masukan nomor polisi dari kendaraan yang ingin dilacak";

  var value = 0;

  AnimationController _con;

  String validateForm;

  bool isLoading = false;

  var resBody;
  String status = "";
  String tglPelanggaran = "";
  String wktPelanggaran = "";
  String nopolisi = "";
  int idKendaraan = 0;
  String catPelanggaran = "";
  String statusPenindakan = "";
  String idKendaraanString = "";
  String fotoDepan = "";
  String fotoKiri = "";
  String fotoKanan = "";
  String fotoBelakang = "";
  String fotoJauh = "";
  String notes = "";
  String kendaraanDerek = "";
  String lokasiInap = "";
  String biayaRetirbusi = "";
  String alamatPengurusan = "";

  Image imageDepan;
  Image imageKiri;
  Image imageKanan;
  Image imageBelakang;
  Image imageJauh;

  String endUrl = "pelanggarans/cek-pelanggaran?no_polisi=";

  getData(String nopol) async {
    String url = Helper.urlApi() + endUrl + nopol;
    var res = await http.get(url, headers: {"Accept": "application/json"});
    var resBody = json.decode(res.body);
    if (resBody["status"] == "ok") {
      status = resBody["status"];
      notes = resBody["message"];
      tglPelanggaran = resBody["items"]["tanggal_pelanggaran"];
      wktPelanggaran = resBody["items"]["waktu_pelanggaran"];
      nopolisi = resBody["items"]["no_polisi"];
      idKendaraan = resBody["items"]["id_jenis_kendaraan"];
      catPelanggaran = resBody["items"]["catatan_pelanggaran"];
      statusPenindakan = resBody["items"]["status_penindakan"];
      fotoDepan = resBody["items"]["foto_depan"];
      fotoBelakang = resBody["items"]["foto_belakang"];
      fotoKiri = resBody["items"]["foto_samping_kiri"];
      fotoKanan = resBody["items"]["foto_samping_kanan"];
      fotoJauh = resBody["items"]["foto_jauh1"];
      kendaraanDerek = resBody["items"]["kendaraan_derek"];
      lokasiInap = resBody["items"]["lokasi_inap"];
      biayaRetirbusi = resBody["items"]["biaya_retribusi"];
      alamatPengurusan = resBody["items"]["alamat_pengurusan"];

      if (catPelanggaran == "") {
        catPelanggaran = "tidak ada catatan";
      } else {
        catPelanggaran = catPelanggaran;
      }
    } else {
      status = resBody["status"];
      notes = resBody["message"];
      tglPelanggaran = "";
      wktPelanggaran = "";
      nopol = "";
      idKendaraan = 0;
      catPelanggaran = "";
      statusPenindakan = "";
      fotoDepan = "";
      fotoKiri = "";
      fotoKanan = "";
      fotoBelakang = "";
      fotoJauh = "";
      kendaraanDerek = "";
      lokasiInap = "";
      biayaRetirbusi = "";
      alamatPengurusan = "";
    }
    convertId();
  }

  check() async {
    final form = _formKey.currentState;
    if (form.validate()) {
      form.save();
      if (validateForm == "true") {
        getData(_nopolController.text);
      } else {
        setState(() {
          isLoading = false;
        });
      }
    } else {
      setState(() {
        isLoading = false;
      });
    }
  }

  void convertId() {
    if (idKendaraan == 1) {
      idKendaraanString = "Kendaraan Roda Dua dan/atau Tiga";
    } else if (idKendaraan == 2) {
      idKendaraanString = "Kendaraan Roda Empat";
    } else {
      idKendaraanString = "Kendaraan Roda Lebih dari Empat";
    }
    convertImage();
  }

  void convertImage() {
    if (fotoDepan != "") {
      imageDepan = Image.network(
        Helper.imageAPI() + fotoDepan,
        width: MediaQuery.of(context).size.width / 2,
      );
      imageBelakang = Image.network(
        Helper.imageAPI() + fotoBelakang,
        width: MediaQuery.of(context).size.width / 2,
      );
      imageKanan = Image.network(
        Helper.imageAPI() + fotoKanan,
        width: MediaQuery.of(context).size.width / 2,
      );
      imageKiri = Image.network(
        Helper.imageAPI() + fotoKiri,
        width: MediaQuery.of(context).size.width / 2,
      );
      imageJauh = Image.network(
        Helper.imageAPI() + fotoJauh,
        width: MediaQuery.of(context).size.width / 2,
      );
    } else {
      imageJauh = Image.network(
        Helper.imageAPI() + fotoJauh,
        width: MediaQuery.of(context).size.width / 2,
      );
    }
    setState(() {
      isLoading = false;
    });
  }

  int toggle = 0;

  void doText() async {
    value = value + 1;

    if (value.isEven) {
      showText =
          "Tekan tombol disebelah kiri dan masukan nomor kendaraan dari kendaraan yang ingin dilacak";
    } else {
      showText = "";
    }
  }

  void doToast() {
    Fluttertoast.showToast(
      msg: "Masukan nomor kendaraan",
      toastLength: Toast.LENGTH_SHORT,
      gravity: ToastGravity.CENTER,
      timeInSecForIosWeb: 1,
      backgroundColor: Colors.red,
      textColor: Colors.white,
      fontSize: 16.0,
    );
  }

  void doToastError() {
    Fluttertoast.showToast(
      msg: "Nomor kendaraan tidak boleh mengandung spasi",
      toastLength: Toast.LENGTH_SHORT,
      gravity: ToastGravity.CENTER,
      timeInSecForIosWeb: 1,
      backgroundColor: Colors.red,
      textColor: Colors.white,
      fontSize: 16.0,
    );
  }

  @override
  void initState() {
    _con = AnimationController(
      vsync: this,
      duration: Duration(milliseconds: 375),
    );
    isLoading = false;
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      resizeToAvoidBottomInset: false,
      appBar: AppBar(
        title: Text("Lacak Pelanggaran"),
        elevation: 0,
      ),
      body: ModalProgressHUD(
        inAsyncCall: isLoading,
        child: Container(
          margin: EdgeInsets.all(24),
          child: Stack(
            children: [
              _hintText(),
              _dataResult(),
              _searchBar(),
            ],
          ),
        ),
      ),
    );
  }

  Widget _hintText() {
    return Align(
      alignment: Alignment.topCenter,
      child: Container(
        margin: EdgeInsets.only(left: 70, top: 10),
        width: MediaQuery.of(context).size.width / 1.2,
        height: 50,
        child: Align(
          alignment: Alignment.center,
          child: Text(
            showText,
            style: text12,
          ),
        ),
      ),
    );
  }

  Widget _searchBar() {
    return Form(
      key: this._formKey,
      child: Padding(
        padding: const EdgeInsets.all(10.0),
        child: Container(
          height: 50.0,
          width: MediaQuery.of(context).size.width,
          alignment: Alignment(-1.0, 0.0),
          child: AnimatedContainer(
            duration: Duration(milliseconds: 375),
            height: 48.0,
            width: (toggle == 0) ? 48.0 : MediaQuery.of(context).size.width,
            curve: Curves.easeOut,
            decoration: BoxDecoration(
              color: Colors.grey,
              borderRadius: BorderRadius.circular(30.0),
              boxShadow: [
                BoxShadow(
                  color: Colors.black26,
                  spreadRadius: -10.0,
                  blurRadius: 10.0,
                  offset: Offset(0.0, 10.0),
                ),
              ],
            ),
            child: Stack(
              children: [
                AnimatedPositioned(
                  duration: Duration(milliseconds: 375),
                  left: (toggle == 0) ? 20.0 : 40.0,
                  curve: Curves.easeOut,
                  top: 11.0,
                  child: AnimatedOpacity(
                    opacity: (toggle == 0) ? 0.0 : 1.0,
                    duration: Duration(milliseconds: 200),
                    child: Container(
                      height: 23.0,
                      width: MediaQuery.of(context).size.width,
                      child: TextFormField(
                        inputFormatters: [
                          LengthLimitingTextInputFormatter(8),
                        ],
                        validator: (value) {
                          if (value.contains(" ")) {
                            doToastError();
                            validateForm = "false";
                            return null;
                          } else if (value.isEmpty) {
                            doToast();
                            validateForm = "false";
                            return null;
                          } else {
                            validateForm = "true";
                            return null;
                          }
                        },
                        controller: _nopolController,
                        cursorRadius: Radius.circular(10.0),
                        cursorWidth: 2.0,
                        cursorColor: pYellow,
                        decoration: InputDecoration(
                          floatingLabelBehavior: FloatingLabelBehavior.never,
                          labelText: 'Lacak Pelanggaran...',
                          labelStyle: TextStyle(
                            fontSize: 15.0,
                            fontWeight: FontWeight.w500,
                          ),
                          alignLabelWithHint: true,
                          border: OutlineInputBorder(
                            borderRadius: BorderRadius.circular(20.0),
                            borderSide: BorderSide.none,
                          ),
                        ),
                      ),
                    ),
                  ),
                ),
                AnimatedPositioned(
                  duration: Duration(milliseconds: 375),
                  top: 6.0,
                  right: 7.0,
                  curve: Curves.easeOut,
                  child: AnimatedOpacity(
                    opacity: (toggle == 0) ? 0.0 : 1.0,
                    duration: Duration(milliseconds: 200),
                    child: Container(
                      padding: EdgeInsets.all(8.0),
                      decoration: BoxDecoration(
                        color: Color(0xffF2F3F7),
                        borderRadius: BorderRadius.circular(30.0),
                      ),
                      child: AnimatedBuilder(
                        child: GestureDetector(
                          child: Icon(
                            Icons.search,
                            size: 20.0,
                            color: Theme.of(context).accentColor,
                          ),
                          onTap: () {
                            setState(() {
                              isLoading = true;
                            });
                            check();
                          },
                        ),
                        builder: (context, widget) {
                          return Transform.rotate(
                            angle: _con.value * 2.0 * pi,
                            child: widget,
                          );
                        },
                        animation: _con,
                      ),
                    ),
                  ),
                ),
                Material(
                  color: Theme.of(context).accentColor,
                  borderRadius: BorderRadius.circular(30.0),
                  child: IconButton(
                    splashRadius: 19.0,
                    icon: Icon(Icons.arrow_right, size: 30, color: white),
                    onPressed: () {
                      doText();
                      setState(
                        () {
                          if (toggle == 0) {
                            toggle = 1;
                            _con.forward();
                          } else {
                            toggle = 0;
                            _nopolController.clear();
                            _con.reverse();
                          }
                        },
                      );
                    },
                  ),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }

  Widget _dataResult() {
    return Container(
      child: idKendaraanString != ""
          ? Container(
              child: status == "ok" ? resBodyOK() : resBodyNO(),
            )
          : Center(
              child: Opacity(
                opacity: 0.2,
                child: Container(
                    width: MediaQuery.of(context).size.width / 5,
                    child: Image(
                      image: AssetImage("images/search.png"),
                    )),
              ),
            ),
    );
  }

  Widget resBodyOK() {
    return Center(
      child: Container(
        child: FlutterTicketWidget(
          width: MediaQuery.of(context).size.width,
          height: MediaQuery.of(context).size.height / 1.5,
          isCornerRounded: true,
          color: Theme.of(context).accentColor,
          child: SingleChildScrollView(
            child: Container(
              padding: EdgeInsets.all(25),
              child: Column(
                children: [
                  Align(
                    alignment: Alignment.topRight,
                    child: Container(
                      padding: EdgeInsets.all(10),
                      child: Text(
                        "$statusPenindakan",
                        style: white15,
                      ),
                    ),
                  ),
                  box20,
                  Text(
                    "$nopolisi",
                    style: white22,
                  ),
                  box10,
                  Text(
                    "$wktPelanggaran",
                    style: white12,
                  ),
                  box30,
                  IntrinsicHeight(
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceAround,
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: [
                        Container(
                          width: MediaQuery.of(context).size.width / 3,
                          child: Text(
                            idKendaraanString,
                            textAlign: TextAlign.center,
                            style: white12,
                          ),
                        ),
                        VerticalDivider(
                          thickness: 5,
                          color: white,
                        ),
                        Container(
                          width: MediaQuery.of(context).size.width / 3,
                          child: Text(
                            "$catPelanggaran",
                            textAlign: TextAlign.center,
                            style: white12,
                          ),
                        ),
                      ],
                    ),
                  ),
                  box20,
                  Text(
                    biayaRetirbusi,
                    textAlign: TextAlign.center,
                    style: white12n,
                  ),
                  box10,
                  Text(
                    alamatPengurusan,
                    textAlign: TextAlign.center,
                    style: white12n,
                  ),
                  box30,
                  showDerek(),
                  Text(
                    "Foto Pelanggaran",
                    textAlign: TextAlign.center,
                    style: white12,
                  ),
                  box10,
                  imageJauh,
                  box10,
                  allImage(),
                ],
              ),
            ),
          ),
        ),
      ),
    );
  }

  Widget showDerek() {
    if (statusPenindakan == "Penderekan") {
      return Column(
        children: [
          Text(
            "Di derek oleh kendaraan derek dengan nomor kendaraan $kendaraanDerek dan lokasi inap di $lokasiInap",
            textAlign: TextAlign.center,
            style: text15,
          ),
          box20,
        ],
      );
    } else {
      return Container();
    }
  }

  Widget allImage() {
    if (fotoDepan != "") {
      return Column(children: [
        Text(
          "Kendaraan Tampak Depan",
          textAlign: TextAlign.center,
          style: white12n,
        ),
        box10,
        imageDepan,
        box10,
        Text(
          "Kendaraan Tampak Samping Kanan",
          textAlign: TextAlign.center,
          style: white12n,
        ),
        box10,
        imageKanan,
        box10,
        Text(
          "Kendaraan Tampak Samping Kiri",
          textAlign: TextAlign.center,
          style: white12n,
        ),
        box10,
        imageKiri,
        box10,
        Text(
          "Kendaraan Tampak Belakang",
          textAlign: TextAlign.center,
          style: white12n,
        ),
        box10,
        imageBelakang,
      ]);
    } else {
      return Container();
    }
  }

  Widget resBodyNO() {
    return Center(
      child: Container(
        height: 200,
        margin: EdgeInsets.all(10),
        padding: EdgeInsets.all(20),
        decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(20),
        ),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            Text(
              notes,
              style: text15,
              textAlign: TextAlign.center,
            ),
            box20,
            Text(
              "Nomor kendaraan yang dicari: " + _nopolController.text,
              style: text12,
              textAlign: TextAlign.center,
            ),
          ],
        ),
      ),
    );
  }
}
