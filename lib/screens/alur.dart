import 'package:derek_masyarakat_v2/components/helper.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';

class Alur extends StatefulWidget {
  @override
  _AlurState createState() => _AlurState();
}

class _AlurState extends State<Alur> {
  bool isLoading = false;
  String judulGambar = "";
  String gambar = "";

  Future infografis() async {
    SharedPreferences pref = await SharedPreferences.getInstance();
    if (pref.getString("status") == "ok") {
      setState(() {
        judulGambar = pref.getString("judulGambar");
        gambar = pref.getString("gambar");
      });
    } else {
      setState(() {
        judulGambar = "";
        gambar = "";
      });
    }
  }
  @override
  void initState() {
    infografis();
    isLoading = true;
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(judulGambar),
        elevation: 0,
      ),
      body: gambar == ""
          ? Container()
          : Stack(
              children: [
                Container(
                  margin: EdgeInsets.all(24),
                  width: MediaQuery.of(context).size.width,
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: [
                      InteractiveViewer(
                        child: Image.network(
                          Helper.infografis() + gambar,
                        ),
                        maxScale: 3.0,
                      ),
                    ],
                  ),
                ),
              ],
            ),
    );
  }
}
