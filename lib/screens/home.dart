import 'package:derek_masyarakat_v2/components/carouselElement.dart';
import 'package:derek_masyarakat_v2/screens/aboutUs.dart';
import 'package:derek_masyarakat_v2/screens/alur.dart';
import 'package:derek_masyarakat_v2/screens/perda.dart';
import 'package:derek_masyarakat_v2/screens/report.dart';
import 'package:derek_masyarakat_v2/screens/search.dart';
import 'package:derek_masyarakat_v2/styles/color.dart';
import 'package:double_back_to_close_app/double_back_to_close_app.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:flutter_speed_dial/flutter_speed_dial.dart';
import 'package:modal_progress_hud/modal_progress_hud.dart';
import 'package:shared_preferences/shared_preferences.dart';

class Home extends StatefulWidget {
  @override
  _HomeState createState() => _HomeState();
}

class _HomeState extends State<Home> with TickerProviderStateMixin {

  ScrollController scrollController;
  bool dialVisible = true;

  bool isLoading = false;

  @override
  void initState() {
    isLoading = false;
    getData();
    super.initState();
    scrollController = ScrollController()
      ..addListener(() {
        setDialVisible(scrollController.position.userScrollDirection ==
            ScrollDirection.forward);
      });
  }

  void setDialVisible(bool value) {
    setState(() {
      dialVisible = value;
    });
  }

  String judul1 = "";
  String judul2 = "";
  String judul3 = "";
  String proses = "";

  Future getData() async {
    SharedPreferences pref = await SharedPreferences.getInstance();
    if (pref.getString("status") == "ok") {
      setState(() {
        judul1 = pref.getString("judul1");
        judul2 = pref.getString("judul2");
        judul3 = pref.getString("judul3");
        proses = pref.getString("proses");
      });
    } else {
      setState(() {
        judul1 = "";
        judul2 = "";
        judul3 = "";
        proses = "";
      });
    }
  }


  @override
  Widget build(BuildContext context) {
    double width = MediaQuery.of(context).size.width;
    return Scaffold(
      resizeToAvoidBottomInset: false,
      resizeToAvoidBottomPadding: false,
      appBar: AppBar(
        elevation: 0,
        title: GestureDetector(
          child: Image.asset(
            "images/newLogo.png",
            width: width / 3.5,
          ),
          onTap: () {
            Navigator.push(
              context,
              MaterialPageRoute(
                builder: (context) => AboutUs(),
              ),
            );
          },
        ),
      ),
      body: ModalProgressHUD(
        inAsyncCall: isLoading,
        child: DoubleBackToCloseApp(
          snackBar: const SnackBar(
            content: Text("Tekan sekali lagi untuk keluar dari aplikasi"),
          ),
          child: SingleChildScrollView(
            child: SafeArea(
              bottom: false,
              child: Stack(
                children: [
                  Container(
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.center,
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: [
                        Container(
                          child: CarouselElement(),
                        ),
                      ],
                    ),
                  ),
                ],
              ),
            ),
          ),
        ),
      ),
      floatingActionButton: buildSpeedDial(),
    );
  }

  SpeedDial buildSpeedDial() {
    return SpeedDial(
      animatedIcon: AnimatedIcons.menu_close,
      animatedIconTheme: IconThemeData(size: 22.0),
      curve: Curves.bounceIn,
      children: [
        SpeedDialChild(
          child: Icon(Icons.list, color: Theme.of(context).dividerColor),
          backgroundColor: dial4,
          onTap: () {
            Navigator.push(
              context,
              MaterialPageRoute(
                builder: (context) => Alur(),
              ),
            );
          },
          label: proses,
          labelStyle: TextStyle(fontWeight: FontWeight.w500),
          labelBackgroundColor: dial4,
        ),
        SpeedDialChild(
          child: Icon(
            Icons.report_problem,
            color: Theme.of(context).dividerColor,
          ),
          backgroundColor: dial3,
          onTap: () {
            Navigator.push(
              context,
              MaterialPageRoute(
                builder: (context) => Report(),
              ),
            );
          },
          label: judul3,
          labelStyle: TextStyle(fontWeight: FontWeight.w500),
          labelBackgroundColor: dial3,
        ),
        SpeedDialChild(
          child: Icon(
            Icons.search,
            color: Theme.of(context).dividerColor,
          ),
          backgroundColor: dial2,
          onTap: () => {
            Navigator.push(
              context,
              MaterialPageRoute(
                builder: (context) => Search(),
              ),
            ),
          },
          label: judul2,
          labelStyle: TextStyle(fontWeight: FontWeight.w500),
          labelBackgroundColor: dial2,
        ),
        SpeedDialChild(
          child: Icon(Icons.note, color: Theme.of(context).dividerColor),
          backgroundColor: dial1,
          onTap: () {
            Navigator.push(
              context,
              MaterialPageRoute(
                builder: (context) => Perda(),
              ),
            );
          },
          label: judul1,
          labelStyle: TextStyle(fontWeight: FontWeight.w500),
          labelBackgroundColor: dial1,
        ),
        
      ],
    );
  }
}
