import 'dart:convert';
import 'dart:io';

import 'package:derek_masyarakat_v2/components/helper.dart';
import 'package:derek_masyarakat_v2/screens/home.dart';
import 'package:derek_masyarakat_v2/styles/style.dart';
import 'package:derek_masyarakat_v2/styles/text.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:geolocator/geolocator.dart';
import 'package:image_cropper/image_cropper.dart';
import 'package:image_picker/image_picker.dart';
import 'package:mime/mime.dart';
import 'package:modal_progress_hud/modal_progress_hud.dart';
import 'package:path/path.dart';
import 'package:http/http.dart' as http;

class Report extends StatefulWidget {
  @override
  _ReportState createState() => _ReportState();
}

class _ReportState extends State<Report> {
  final _formKey = GlobalKey<FormState>();
  TextEditingController _nameController = TextEditingController();
  TextEditingController _phoneController = TextEditingController();
  TextEditingController _locationController = TextEditingController();
  TextEditingController _notesController = TextEditingController();

  bool isLoading = false;

  bool status;
  String note = "";
  String imgData;
  String statusImg;

  Future<File> imagee;
  String base64Image;
  File image;
  File cropped;
  File _selectedFile;

  double _lat;
  double _long;

  _getCurrentLocation() async {
    Position position = await Geolocator.getCurrentPosition(
      desiredAccuracy: LocationAccuracy.high,
    );
    _lat = position.latitude;
    _long = position.longitude;
    upload();
  }

  check() async {
    final form = _formKey.currentState;
    if (form.validate()) {
      form.save();
      _getCurrentLocation();
    } else {
      setState(() {
        isLoading = false;
      });
      _dialogValidate();
    }
  }

  // API UNTUK DEREK
  Future submit() async {
    var mobileToken = Helper.mobileToken();
    var url =
        Uri.parse(Helper.urlApi() + "pelanggarans/post-laporan-masyarakat");

    final uploadRequest = http.MultipartRequest('POST', url);
    uploadRequest.headers["app_mobile_token"] = mobileToken;
    final file = http.MultipartFile.fromString("foto1", imgData);
    uploadRequest.files.add(file);
    uploadRequest.fields["nama_pelapor"] = _nameController.text;
    uploadRequest.fields["lat"] = _lat.toString();
    uploadRequest.fields["long"] = _long.toString();
    uploadRequest.fields["lokasi_pelanggaran"] = _locationController.text;
    uploadRequest.fields["nomor_telepon"] = _phoneController.text;
    uploadRequest.fields["keterangan_pelanggaran"] = _notesController.text;

    try {
      final streamedResponse = await uploadRequest.send();
      final response = await http.Response.fromStream(streamedResponse);
      if (response.statusCode == 200) {
        final responseData = json.decode(response.body);
        status = responseData[0]["status"];
        note = responseData[0]["msg"];
        setState(() {
          isLoading = false;
        });
        _dialogPeringatan();
        return responseData;
      } else {
        setState(() {
          isLoading = false;
        });
        _dialogGagal();
      }
    } catch (e) {
      setState(() {
        isLoading = false;
      });
      _dialogGagal();
    }
  }

  @override
  void initState() {
    super.initState();
    isLoading = false;
  }

  getImage(ImageSource source) async {
    // ignore: deprecated_member_use
    image = await ImagePicker.pickImage(source: source);
    if (image != null) {
      cropped = await ImageCropper.cropImage(
        sourcePath: image.path,
        aspectRatioPresets: Platform.isAndroid
            ? [
                CropAspectRatioPreset.square,
                CropAspectRatioPreset.ratio3x2,
                CropAspectRatioPreset.original,
                CropAspectRatioPreset.ratio4x3,
                CropAspectRatioPreset.ratio16x9
              ]
            : [
                CropAspectRatioPreset.original,
                CropAspectRatioPreset.square,
                CropAspectRatioPreset.ratio3x2,
                CropAspectRatioPreset.ratio4x3,
                CropAspectRatioPreset.ratio5x3,
                CropAspectRatioPreset.ratio5x4,
                CropAspectRatioPreset.ratio7x5,
                CropAspectRatioPreset.ratio16x9
              ],
        compressQuality: 100,
        androidUiSettings: AndroidUiSettings(
          initAspectRatio: CropAspectRatioPreset.original,
          lockAspectRatio: false,
          toolbarColor: Theme.of(this.context).canvasColor,
          activeControlsWidgetColor: Theme.of(this.context).accentColor,
          toolbarTitle: "Sesuaikan Gambar",
          toolbarWidgetColor: Theme.of(this.context).dividerColor,
          backgroundColor: Theme.of(this.context).canvasColor,
        ),
      );
      this.setState(() {
        _selectedFile = cropped;
      });
    }
  }

  upload() async {
    if (null == image) {
      imgData = "";
      submit();
    } else if (null != image) {
      base64Image = base64Encode(image.readAsBytesSync());
      String filename = basename(image.path);
      String mime = lookupMimeType(filename);
      imgData = "data:" + mime + ";base64," + base64Image;
      submit();
    } else {}
    return imgData;
  }

  setStatus(String message) {
    setState(() {
      statusImg = message;
    });
  }

  @override
  Widget build(BuildContext context) {
    final f2 = InputDecoration(
      focusedBorder: OutlineInputBorder(
        borderSide: BorderSide(
          color: Theme.of(this.context).dividerColor,
        ),
      ),
      enabledBorder: OutlineInputBorder(
        borderSide: BorderSide(
          color: Theme.of(this.context).dividerColor,
        ),
      ),
    );

    return Scaffold(
      resizeToAvoidBottomInset: false,
      resizeToAvoidBottomPadding: false,
      appBar: AppBar(
        title: Text("Laporkan Pelanggaran"),
        elevation: 0,
      ),
      body: ModalProgressHUD(
        inAsyncCall: isLoading,
        child: Stack(
          children: [
            SingleChildScrollView(
              physics: AlwaysScrollableScrollPhysics(),
              padding: EdgeInsets.symmetric(
                horizontal: 15.0,
                vertical: 20.0,
              ),
              child: Form(
                key: this._formKey,
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.start,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Row(
                      children: [
                        Text("Nama Lengkap"),
                        Text(" *", style: star),
                      ],
                    ),
                    Container(
                      width: MediaQuery.of(context).size.width,
                      child: TextFormField(
                        maxLength: 200,
                        controller: _nameController,
                        keyboardType: TextInputType.text,
                        decoration: f2,
                        validator: (value) {
                          if (value.isEmpty) {
                            return "Nama Pelapor Tidak Boleh Kosong";
                          } else {
                            return null;
                          }
                        },
                      ),
                    ),
                    box10,
                    Row(
                      children: [
                        Text("Nomor Telepon"),
                        Text(" *", style: star),
                      ],
                    ),
                    Container(
                      width: MediaQuery.of(context).size.width,
                      child: TextFormField(
                        maxLength: 14,
                        controller: _phoneController,
                        keyboardType: TextInputType.number,
                        decoration: f2,
                        validator: (value) {
                          if (value.isEmpty) {
                            return "Nomor Telepon Tidak Boleh Kosong";
                          } else {
                            return null;
                          }
                        },
                      ),
                    ),
                    box10,
                    Row(
                      children: [
                        Text("Lokasi Pelanggaran"),
                        Text(" *", style: star),
                      ],
                    ),
                    Container(
                      width: MediaQuery.of(context).size.width,
                      child: TextFormField(
                        maxLines: 1,
                        maxLength: 100,
                        controller: _locationController,
                        keyboardType: TextInputType.text,
                        decoration: f2,
                        validator: (value) {
                          if (value.isEmpty) {
                            return "Lokasi Pelanggaran Tidak Boleh Kosong";
                          } else {
                            return null;
                          }
                        },
                      ),
                    ),
                    box10,
                    Row(
                      children: [
                        Text("Keterangan Pelanggaran"),
                        Text(" *", style: star),
                      ],
                    ),
                    Container(
                      width: MediaQuery.of(context).size.width,
                      child: TextFormField(
                        maxLines: 3,
                        maxLength: 200,
                        controller: _notesController,
                        keyboardType: TextInputType.text,
                        decoration: f2,
                        validator: (value) {
                          if (value.isEmpty) {
                            return "Keterangan Pelanggaran Tidak Boleh Kosong";
                          } else {
                            return null;
                          }
                        },
                      ),
                    ),
                    box10,
                    Text("Masukan Gambar"),
                    Container(
                      child: getImageWidget(),
                    ),
                    box30,
                    Container(
                      width: MediaQuery.of(context).size.width,
                      child: RaisedButton(
                        padding: EdgeInsets.only(top: 15, bottom: 15),
                        color: Theme.of(context).accentColor,
                        onPressed: () {
                          _dialogPersetujuan();
                        },
                        child: Text(
                          "Kirim",
                          style: white13,
                        ),
                      ),
                    ),
                  ],
                ),
              ),
            )
          ],
        ),
      ),
    );
  }

  Widget getImageWidget() {
    if (_selectedFile != null) {
      return Center(
        child: Column(
          children: [
            Image.file(
              _selectedFile,
            ),
            RaisedButton(
              child: Text(
                "Ubah Gambar",
                style: TextStyle(color: Theme.of(this.context).canvasColor),
              ),
              onPressed: () async {
                _dialogChoice(this.context);
              },
              color: Theme.of(this.context).dividerColor,
            ),
          ],
        ),
      );
    } else {
      return GestureDetector(
        child: Container(
          width: MediaQuery.of(this.context).size.width,
          height: MediaQuery.of(this.context).size.height / 4,
          padding: EdgeInsets.all(10),
          decoration: BoxDecoration(
            border: Border.all(
              color: Theme.of(this.context).dividerColor,
            ),
          ),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              Icon(
                Icons.photo_camera,
                size: 20,
              ),
              Text(
                "Gambar Pelanggaran",
                style: text12,
                textAlign: TextAlign.center,
              ),
            ],
          ),
        ),
        onTap: () {
          _dialogChoice(this.context);
        },
      );
    }
  }

  //ALERT DIALOG
  Future _dialogChoice(BuildContext context) {
    return showDialog(
      context: context,
      builder: (BuildContext context) {
        return AlertDialog(
          title: Text("Unggah gambar melalui"),
          content: SingleChildScrollView(
            child: ListBody(
              children: [
                GestureDetector(
                  child: Text("Gallery"),
                  onTap: () {
                    getImage(ImageSource.gallery);
                    setStatus('');
                    Navigator.of(context).pop();
                  },
                ),
                SizedBox(height: 20),
                GestureDetector(
                  child: Text("Kamera"),
                  onTap: () {
                    getImage(ImageSource.camera);
                    setStatus('');
                    Navigator.of(context).pop();
                  },
                ),
              ],
            ),
          ),
        );
      },
    );
  }

  Future _dialogGagal() {
    return showDialog(
      context: this.context,
      barrierDismissible: false,
      builder: (BuildContext context) {
        return AlertDialog(
          shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(25),
          ),
          content: Text(
            "Maaf terjadi kesalahan, silahkan ulangi beberapa saat lagi",
            textAlign: TextAlign.center,
          ),
          actions: [
            Container(
              width: MediaQuery.of(context).size.width,
              height: 50,
              padding: EdgeInsets.all(8),
              child: FlatButton(
                color: Theme.of(context).accentColor,
                child: Text("Coba Lagi"),
                shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(18),
                ),
                onPressed: () {
                  Navigator.of(context).pop();
                },
              ),
            ),
          ],
        );
      },
    );
  }

  Future _dialogValidate() {
    return showDialog(
      context: this.context,
      barrierDismissible: false,
      builder: (BuildContext context) {
        return AlertDialog(
          shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(25),
          ),
          content: Text(
            "Mohon masukan data laporan secara lengkap",
            textAlign: TextAlign.center,
          ),
          actions: [
            Container(
              width: MediaQuery.of(context).size.width,
              height: 50,
              child: Row(
                children: [
                  Expanded(
                    child: Padding(
                      padding: EdgeInsets.all(8),
                      child: FlatButton(
                        child: Text("Kembali ke halaman laporan"),
                        color: Theme.of(context).accentColor,
                        shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(18),
                          side: BorderSide(
                            color: Theme.of(context).accentColor,
                          ),
                        ),
                        onPressed: () {
                          Navigator.of(context).pop();
                        },
                      ),
                    ),
                  ),
                ],
              ),
            ),
          ],
        );
      },
    );
  }

  Future _dialogPersetujuan() {
    return showDialog(
      context: this.context,
      barrierDismissible: false,
      builder: (BuildContext context) {
        return AlertDialog(
          shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(25),
          ),
          title: Text(
            "Terima kasih telah berparitisipasi dengan cara melaporkan pelanggaran!",
            style: text15,
          ),
          content: Text(
            "Nama dan nomor telepon akan dirahasiakan. Jika anda sudah yakin dengan laporan pelanggaran, silahkan pilih 'Ya'",
            style: text12n,
          ),
          actions: [
            Container(
              width: MediaQuery.of(context).size.width,
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceAround,
                children: [
                  Container(
                    width: MediaQuery.of(context).size.width / 3,
                    child: FlatButton(
                      color: Theme.of(context).accentColor,
                      child: Text("Ya"),
                      onPressed: () {
                        Navigator.of(context).pop();
                        setState(() {
                          isLoading = true;
                        });
                        check();
                      },
                    ),
                  ),
                  Container(
                    width: MediaQuery.of(context).size.width / 3,
                    child: FlatButton(
                      color: Colors.red,
                      child: Text("Tidak"),
                      onPressed: () {
                        Navigator.of(context).pop();
                      },
                    ),
                  ),
                ],
              ),
            ),
          ],
        );
      },
    );
  }

  Future _dialogPeringatan() {
    return showDialog(
      context: this.context,
      barrierDismissible: false,
      builder: (BuildContext context) {
        return WillPopScope(
          onWillPop: () async => false,
          child: AlertDialog(
            shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(25),
            ),
            content: Text(
              note,
              textAlign: TextAlign.center,
            ),
            actions: [
              Container(
                width: MediaQuery.of(context).size.width,
                height: 50,
                padding: EdgeInsets.all(8),
                child: FlatButton(
                  color: Theme.of(context).accentColor,
                  child: Text("Kembali ke halaman utama"),
                  shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(18),
                  ),
                  onPressed: () {
                    Navigator.pushAndRemoveUntil(
                      context,
                      MaterialPageRoute(builder: (context) => Home()),
                      (Route<dynamic> route) => false,
                    );
                  },
                ),
              ),
            ],
          ),
        );
      },
    );
  }
}
