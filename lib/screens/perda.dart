import 'package:derek_masyarakat_v2/components/helper.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';

class Perda extends StatefulWidget {
  @override
  _PerdaState createState() => _PerdaState();
}

class _PerdaState extends State<Perda> {
  String gambarPerda = "";

  Future getData() async {
    SharedPreferences pref = await SharedPreferences.getInstance();
    if (pref.getString("status") == "ok") {
      setState(() {
        gambarPerda = pref.getString("gambarPerda");
      });
    } else {
      setState(() {
        gambarPerda = "";
      });
    }
  }

  @override
  void initState() {
    getData();
    super.initState();
  }

  @override
  void dispose() {
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      resizeToAvoidBottomInset: false,
      resizeToAvoidBottomPadding: false,
      appBar: AppBar(
        title: Text(
          "Peraturan Daerah",
        ),
        elevation: 0,
      ),
      body: gambarPerda == ""
          ? Container()
          : SingleChildScrollView(
              child: Container(
                margin: EdgeInsets.all(24),
                child: Image.network(
                  Helper.infografis() + gambarPerda,
                ),
              ),
            ),
    );
  }
}
