import 'dart:async';
import 'dart:convert';
import 'package:derek_masyarakat_v2/components/helper.dart';
import 'package:derek_masyarakat_v2/screens/home.dart';
import 'package:derek_masyarakat_v2/screens/onboarding.dart';
import 'package:derek_masyarakat_v2/styles/color.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:http/http.dart' as http;

class SplashScreen extends StatefulWidget {
  @override
  _SplashScreenState createState() => _SplashScreenState();
}

class _SplashScreenState extends State<SplashScreen> {
  bool isSign = false;

  Future _cekSign() async {
    SharedPreferences pref = await SharedPreferences.getInstance();
    if (pref.getBool("isSign") == null) {
      Navigator.pushAndRemoveUntil(
        context,
        MaterialPageRoute(builder: (context) => Onboarding()),
        (Route<dynamic> route) => false,
      );
    } else if (pref.getBool("isSign") == true) {
      Navigator.pushAndRemoveUntil(
        context,
        MaterialPageRoute(builder: (context) => Home()),
        (Route<dynamic> route) => false,
      );
    } else {}
  }

  String judul1;
  String desc1;
  String judul2;
  String desc2;
  String judul3;
  String desc3;
  String status;
  String judulGambar;
  String gambar;
  String gambarPerda;
  String proses;

  Future getAbout() async {
    var url = Helper.urlApi() + "masters/get-home-info";
    var response = await http.get(url, headers: {"Accept": "application/json"});
    var _jsondata = json.decode(response.body);
    if (_jsondata["status"] == "ok") {
      status = _jsondata["status"];
      judul1 = _jsondata["items"][0]["judul"];
      desc1 = _jsondata["items"][0]["deskripsi"];
      judul2 = _jsondata["items"][1]["judul"];
      desc2 = _jsondata["items"][1]["deskripsi"];
      judul3 = _jsondata["items"][2]["judul"];
      desc3 = _jsondata["items"][2]["deskripsi"];
      judulGambar = _jsondata["items2"][1]["content"];
      gambar = _jsondata["items2"][1]["image"];
      gambarPerda = _jsondata["items2"][0]["image"];
      proses = _jsondata["items2"][1]["content"];
    } else {
      status = "";
      judul1 = "";
      desc1 = "";
      judul2 = "";
      desc2 = "";
      judul3 = "";
      desc3 = "";
      judulGambar = "";
      gambar = "";
      gambarPerda = "";
      proses = "";
    }
    _setPref();
  }

  void _setPref() async {
    SharedPreferences pref = await SharedPreferences.getInstance();
    pref.setString("status", status);
    pref.setString("judul1", judul1);
    pref.setString("desc1", desc1);
    pref.setString("judul2", judul2);
    pref.setString("desc2", desc2);
    pref.setString("judul3", judul3);
    pref.setString("desc3", desc3);
    pref.setString("judulGambar", judulGambar);
    pref.setString("gambar", gambar);
    pref.setString("gambarPerda", gambarPerda);
    pref.setString("proses", proses);
    _cekSign();
  }

  @override
  void initState() {
    getAbout();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Stack(
      children: [
        Container(
          color: blue,
        ),
        Align(
          alignment: Alignment.center,
          child: Image.asset(
            "images/newLogoWhite.png",
            width: MediaQuery.of(context).size.width / 2,
          ),
        ),
      ],
    );
  }
}
