import 'dart:convert';
import 'package:derek_masyarakat_v2/components/helper.dart';
import 'package:derek_masyarakat_v2/styles/style.dart';
import 'package:derek_masyarakat_v2/styles/text.dart';
import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'package:modal_progress_hud/modal_progress_hud.dart';
import 'package:url_launcher/url_launcher.dart';

class AboutUs extends StatefulWidget {
  @override
  _AboutUsState createState() => _AboutUsState();
}

class _AboutUsState extends State<AboutUs> {
  String appName = "";
  String version = "";
  String releaseDate = "";
  String description = "";
  String contact = "";
  String status = "";
  String phoneNumber = "";
  String instagram = "";
  String email = "";
  var resBody = "";

  String endUrl = "pelanggarans/get-about-app";

  bool isLoading = false;

  getData() async {
    var mobileToken = Helper.mobileToken();
    String url = Helper.urlApi() + endUrl;
    var res = await http.get(url, headers: {"app_mobile_token": mobileToken});
    var resBody = json.decode(res.body);

    if (resBody["status"] == "ok") {
      status = resBody["status"];
      appName = resBody["items"]["app_name"];
      version = resBody["items"]["version"];
      releaseDate = resBody["items"]["release_date"];
      contact = resBody["items"]["contact"];
      description = resBody["items"]["description"];
      phoneNumber = resBody["items"]["wa"];
      instagram = resBody["items"]["ig"];
      email = resBody["items"]["email"];
    } else {
      status = "";
      appName = "";
      version = "";
      releaseDate = "";
      contact = "";
      description = "";
      phoneNumber = "";
      instagram = "";
      email = "";
    }
    setState(() {
      isLoading = false;
    });
  }

  void openWhatsapp() async {
    var urlPhone = "https://wa.me/" + phoneNumber;
    if (await canLaunch(urlPhone)) {
      launch(urlPhone);
    } else {
      throw 'Tidak dapat';
    }
  }

  void openIG() async {
    var urlIG = "https://www.instagram.com/_u/" + instagram;
    if (await canLaunch(urlIG)) {
      launch(urlIG);
    } else {
      throw 'Tidak dapat membuka website';
    }
  }

  void openEmail() async {
    var urlEmail = "mailto:" + email;
    if (await canLaunch(urlEmail)) {
      launch(urlEmail);
    } else {
      throw 'Tidak dapat';
    }
  }

  @override
  void initState() {
    getData();
    isLoading = true;

    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      resizeToAvoidBottomInset: false,
      resizeToAvoidBottomPadding: false,
      appBar: AppBar(
        title: Text("Tentang Aplikasi"),
        elevation: 0,
      ),
      body: ModalProgressHUD(
        inAsyncCall: isLoading,
        child: status != ""
            ? Container(
                margin: EdgeInsets.all(24),
                child: Stack(
                  children: [
                    Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Container(
                          child: Text(
                            description,
                            style: text12,
                          ),
                        ),
                        box20,
                        contactPerson(),
                        box20,
                      ],
                    ),
                    Align(
                      alignment: Alignment.bottomLeft,
                      child: Text(
                        "Versi aplikasi : " + version,
                        style: TextStyle(color: Theme.of(context).dividerColor),
                      ),
                    ),
                  ],
                ),
              )
            : Container(),
      ),
    );
  }

  Widget contactPerson() {
    return Center(
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          Text(
            contact,
            style: text12,
          ),
          box10,
          GestureDetector(
            child: Container(
              width: MediaQuery.of(context).size.width,
              height: 50,
              child: Padding(
                padding: const EdgeInsets.all(8.0),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    boxW5,
                    Image.asset(
                      "images/whatsapp.png",
                      width: MediaQuery.of(context).size.width / 15,
                    ),
                    Text(
                      " " + phoneNumber,
                      style: white12,
                    ),
                  ],
                ),
              ),
              decoration: BoxDecoration(
                color: Theme.of(context).accentColor,
                borderRadius: BorderRadius.all(
                  Radius.circular(20),
                ),
              ),
            ),
            onTap: () {
              openWhatsapp();
            },
          ),
          box5,
          GestureDetector(
            child: Container(
              width: MediaQuery.of(context).size.width,
              height: 50,
              child: Padding(
                padding: const EdgeInsets.all(8.0),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    boxW5,
                    Image.asset(
                      "images/instagram.png",
                      width: MediaQuery.of(context).size.width / 15,
                    ),
                    Text(
                      " " + instagram,
                      style: white12,
                    ),
                  ],
                ),
              ),
              decoration: BoxDecoration(
                color: Theme.of(context).accentColor,
                borderRadius: BorderRadius.all(
                  Radius.circular(20),
                ),
              ),
            ),
            onTap: () {
              openIG();
            },
          ),
          box5,
          GestureDetector(
            child: Container(
              width: MediaQuery.of(context).size.width,
              height: 50,
              child: Padding(
                padding: const EdgeInsets.all(8.0),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    boxW5,
                    Image.asset(
                      "images/email.png",
                      width: MediaQuery.of(context).size.width / 15,
                    ),
                    Text(
                      " " + email,
                      style: white12,
                    ),
                  ],
                ),
              ),
              decoration: BoxDecoration(
                color: Theme.of(context).accentColor,
                borderRadius: BorderRadius.all(
                  Radius.circular(20),
                ),
              ),
            ),
            onTap: () {
              openEmail();
            },
          ),
        ],
      ),
    );
  }
}
