import 'package:derek_masyarakat_v2/screens/splashScreen.dart';
import 'package:derek_masyarakat_v2/styles/color.dart';
import 'package:flutter/material.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      themeMode: ThemeMode.system,
      theme: ThemeData(
        brightness: Brightness.light,
        accentColor: blue,
        primaryColor: white,
        dividerColor: black,
        canvasColor: white,
        
      ),
      darkTheme: ThemeData(
        brightness: Brightness.dark,
        accentColor: blue,
        primaryColor: black,
        dividerColor: white,
        canvasColor: black,
      ),
      debugShowCheckedModeBanner: false,
      home: SplashScreen(),
    );
  }
}
